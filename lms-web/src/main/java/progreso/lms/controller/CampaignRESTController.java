package progreso.lms.controller;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import progreso.lms.domain.CampaignDto;
import progreso.lms.service.CampainService;
import progreso.web.ApplicationController;

@Controller
@RequestMapping("/api/campaign")
public class CampaignRESTController extends ApplicationController {

	private final Logger LOG = LoggerFactory.getLogger(CampaignRESTController.class);
	
	@Autowired
	private CampainService campaignService;
	
	@RequestMapping(method=RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List> list() {
       
		if( LOG.isDebugEnabled() ){
			LOG.debug("Listing all campaigns...");
		}
		
		List<CampaignDto>results = this.campaignService.list();

		return new ResponseEntity<List>(results, HttpStatus.OK);
    }
 
	@RequestMapping(value = "/{id}", method=RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<CampaignDto> view(@PathVariable("id")Long id) {
        
		if( LOG.isDebugEnabled() ){
			LOG.debug("Retrieving campaign: id=>"+ id);
		}
		
		CampaignDto campaignDto;
		campaignDto = campaignService.view(id);
		
		return new ResponseEntity<CampaignDto>(campaignDto, HttpStatus.OK);
    }
	
}

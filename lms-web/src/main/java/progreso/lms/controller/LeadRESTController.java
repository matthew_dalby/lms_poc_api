package progreso.lms.controller;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import progreso.lms.common.EntityValidationException;
import progreso.lms.domain.LeadDTO;
import progreso.lms.service.LeadService;
import progreso.web.ApplicationController;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RestController
@RequestMapping(value="/api/lead")
public class LeadRESTController extends ApplicationController{

	private final Logger LOG = LoggerFactory.getLogger(LeadRESTController.class);
	
	@Autowired
	private LeadService leadService;
	
	@RequestMapping(method = RequestMethod.GET)
    @ResponseBody
	public Map<String, Object>search(@RequestParam(required=false) String firstName,@RequestParam(required=false) String lastName){
		if( LOG.isDebugEnabled() ){
            LOG.debug("Controller called, listing lead entities");
        }
		
		List<LeadDTO> leads;
		
		/**
		 * If search criteria is provided, then search is performed, otherwise we perform a basic list operation.
		 */
		if( leadService.searchCriteriaPresent(firstName, lastName) ){
			leads = leadService.search(firstName, lastName);
		}
		else{
			leads = leadService.list();
		}
		
		Map<String, Object>resultData = new HashMap();
        resultData.put("data", leads);

        return resultData;
		
	}
	
	@RequestMapping(method = RequestMethod.GET, value="/{id}")
    @ResponseBody
	public Map<String, Object>view(@PathVariable("id") BigInteger id){
		if( LOG.isDebugEnabled() ){
            LOG.debug("Controller called, displaying selected lead");
        }
		
		LeadDTO lead;
		lead = leadService.view(id);
		
		Map<String, Object>resultData = new HashMap();
        resultData.put("data", lead);

        return resultData;
		
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public Map<String, Object> create(@Valid @RequestBody LeadDTO lead,  BindingResult bindingResult){
		if( LOG.isDebugEnabled() ){
			LOG.debug("Controller called: Creating new lead.");
		}

		if( bindingResult.hasErrors() ){
			throw new EntityValidationException(lead, bindingResult);
		}

		lead = leadService.create(lead);

		Map<String, Object>resultData = new HashMap();
		resultData.put("data", lead);

		return resultData;
	}
	
	@RequestMapping(method = RequestMethod.PUT)
	public Map<String, Object> update(@Valid @RequestBody LeadDTO lead,  BindingResult bindingResult){
		if( LOG.isDebugEnabled() ){
			LOG.debug("Controller called: Updating existing lead.");
		}

		if( bindingResult.hasErrors() ){
			throw new EntityValidationException(lead, bindingResult);
		}

		lead = leadService.update(lead);

		Map<String, Object>resultData = new HashMap();
		resultData.put("data", lead);

		return resultData;
	}
	
	
}

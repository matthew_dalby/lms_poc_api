package progreso.lms.controller;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import progreso.lms.service.LocalEnvironmentService;

@Controller
public class HeartbeatController {
	@Autowired
	private LocalEnvironmentService environmentService;
	
	private final Logger LOG = LoggerFactory.getLogger(HeartbeatController.class); 
	
	@RequestMapping("/HeartBeat")
	@ResponseBody
	public HashMap getStatus(){
		
		HashMap<String,String> environment;
		environment = environmentService.getEnvironmentConfig();

		
		return environment;
	}
}

package progreso.lms.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import progreso.lms.persistence.entity.LeadProfile;
import progreso.lms.service.LeadProfileService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RestController
@RequestMapping(value="/api/lead_profile")
public class LeadProfileRESTController {

	private final Logger LOG = LoggerFactory.getLogger(LeadProfileRESTController.class);
	
	@Autowired
	private LeadProfileService leadProfileService;
	
	@RequestMapping(method = RequestMethod.GET)
    @ResponseBody
	public Map<String, Object>list(){
		if( LOG.isDebugEnabled() ){
            LOG.debug("Controller called");
        }
		
		List<LeadProfile> leads;
		leads = leadProfileService.list();
		
		Map<String, Object>resultData = new HashMap();
        resultData.put("data", leads);

        return resultData;
		
	}
	
	@RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
	public Map<String, Object>create(@RequestBody LeadProfile lead){
		if( LOG.isDebugEnabled() ){
            LOG.debug("Controller called");
        }
		
		lead = this.leadProfileService.save(lead);
		
		Map<String, Object>resultData = new HashMap();
		resultData.put("data", lead);

        return resultData;
	}
	
}

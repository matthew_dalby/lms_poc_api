package progreso.lms.persistence.repo;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.util.Assert;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import progreso.lms.persistence.entity.Campaign;

@ContextConfiguration(locations = "classpath:META-INF/test-context-core.xml")
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class,
    TransactionalTestExecutionListener.class,
    DbUnitTestExecutionListener.class })
@DatabaseSetup("/default-data-set.xml")
public class CampaignRepoTest {

	@Autowired
	private CampaignRepo repo;
	
	@Test
	public void shouldFindAllCorrectly(){
		
		List<Campaign>campaigns = repo.findAll();
		
		Assert.notNull(campaigns);
		Assert.notEmpty(campaigns);
		Assert.isTrue(campaigns.size()==3);
		
	}
	
}

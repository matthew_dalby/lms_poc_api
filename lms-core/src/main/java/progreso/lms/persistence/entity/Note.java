package progreso.lms.persistence.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import java.math.BigInteger;
import java.sql.Timestamp;

//@Entity
//@Table(name="note")
public class Note {

	/************************************************************************************************
	 * Attributes
	 ************************************************************************************************/
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="note_id")
	private String noteId;

	//TODO: Enforce size constraints on mysql 'text' type field; need to locate maximum length
	@Column(name="body")
	private String body;

	@Column(name="created_at")
	private Timestamp createdAt;

	@Column(name="create_user_id")
	private BigInteger createUserId;

	@ManyToOne
	@JoinColumn(name = "lead_id")
	@JsonBackReference
	private LeadProfile lead;

	@Size( max=10, message="Value for field 'note type' A cannot exceed 10 characters.")
	@Column(name="note_type")
	private String type;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	@Column(name="update_user_id")
	private BigInteger updateUserId;

	/************************************************************************************************
	 * Getters/setters
	 ************************************************************************************************/
	public String getNoteId() {
		return noteId;
	}

	public void setNoteId(String noteId) {
		this.noteId = noteId;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public Timestamp getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public BigInteger getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(BigInteger createUserId) {
		this.createUserId = createUserId;
	}

	public LeadProfile getLead() {
		return lead;
	}

	public void setLead(LeadProfile lead) {
		this.lead = lead;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		if(type!=null){
			type = type.toUpperCase();
		}
		this.type = type;
	}

	public Timestamp getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public BigInteger getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(BigInteger updateUserId) {
		this.updateUserId = updateUserId;
	}

	/************************************************************************************************
	 * Convenience methods
	 ************************************************************************************************/
	
	
	/************************************************************************************************
	 * Persistence hooks
	 ************************************************************************************************/
	
	
	
	
}

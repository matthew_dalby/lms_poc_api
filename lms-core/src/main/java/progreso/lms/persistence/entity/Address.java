package progreso.lms.persistence.entity;

import java.util.Date;
import java.math.BigInteger;
import java.sql.Timestamp;
import javax.persistence.Column; 
import javax.persistence.Entity;
import javax.persistence.GeneratedValue; 
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.SecondaryTable;
import javax.validation.constraints.Size;
import javax.persistence.PrimaryKeyJoinColumn;

import com.fasterxml.jackson.annotation.JsonBackReference;

//@Entity
//@Table(name="lead_address")
//@SecondaryTable(name = "lead_address", pkJoinColumns=@PrimaryKeyJoinColumn(name="address_id", referencedColumnName="address_id"))
public class Address {
	
	/************************************************************************************************
	 * Attributes
	 ************************************************************************************************/
	//@Id
	//@GeneratedValue(strategy=GenerationType.IDENTITY)
	//@Column(name="address_id")
	private BigInteger addressId;
	
	//@Column(name="is_active")
	private boolean active;

	//@Column(name="cass_verified")
	private boolean cassVerified;

	@Size( max=100, message="Value for field 'city' A cannot exceed 100 characters.")
	//@Column(name="city")
	private String city;

	@Size( max=50, message="Value for field 'country' A cannot exceed 50 characters.")
	//@Column(name="country")
	private String country;

	//@Column(name="created_at")
	private Timestamp createdAt;

	//@Column(name="create_user_id")
	private BigInteger createUserId;

	@Size( max=255, message="Value for field 'street address 1' A cannot exceed 255 characters.")
	//@Column(name="street_address_1")
	private String streetAddress1;

	@Size( max=255, message="Value for field 'street address 2' A cannot exceed 255 characters.")
	//@Column(name="street_address_2")
	private String streetAddress2;
						
	//@Column(name="latitude")
	private double latitude;					

	//@ManyToOne(optional=false)
	//@JoinColumn(name = "lead_id")
	//@JsonBackReference
	private LeadProfile lead;

	//@Column(name="longitude")
	private double longitude;	

	@Size( max=16, message="Value for field 'postal Code' A cannot exceed 16 characters.")
	//@Column(name="postal_code")
	private String postalCode;

	@Size( max=2, message="Value for field 'state' A cannot exceed 2 characters.")
	//@Column(name="state")
	private String state;
	
	@Size( max=2, message="Value for field 'type' A cannot exceed 2 characters.")
	//@Column(name="type")
	private String type;

	//@Column(name="updated_at")
	private Timestamp updatedAt;			

	//@Column(name="update_user_id")
	private BigInteger updateUserId;		

	@Size( max=255, message="")
	//@Column(name="cass_street_address_1")
	private String cassStreetAddress1;
	
	@Size( max=255, message="")
	//@Column(name="cass_street_address_2")
	private String cassStreetAddress2;
	
	@Size( max=255, message="")
	//@Column(name="cass_city")
	private String cassCity;
	
	@Size( max=255, message="")
	//@Column(name="cass_state")
	private String cassState;
	
	@Size( max=255, message="")
	//@Column(name="cass_country")
	private String cassCountry;
	
	@Size( max=255, message="")
	//@Column(name="cass_postal_code")
	private String cassPostalCode;
	
	@Size( max=255, message="")
	//@Column(name="transaction_id")
	private BigInteger transactionId;
	
	
	/************************************************************************************************
	 * Getter/setters
	 ************************************************************************************************/
	public BigInteger getAddressId() {
		return addressId;
	}

	public void setAddressId(BigInteger addressId) {
		this.addressId = addressId;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean isCassVerified() {
		return cassVerified;
	}

	public void setCassVerified(boolean cassVerified) {
		this.cassVerified = cassVerified;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Timestamp getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public BigInteger getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(BigInteger createUserId) {
		this.createUserId = createUserId;
	}

	public String getStreetAddress1() {
		return streetAddress1;
	}

	public void setStreetAddress1(String streetAddress1) {
		this.streetAddress1 = streetAddress1;
	}

	public String getStreetAddress2() {
		return streetAddress2;
	}

	public void setStreetAddress2(String streetAddress2) {
		this.streetAddress2 = streetAddress2;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public LeadProfile getLead() {
		return lead;
	}

	public void setLead(LeadProfile lead) {
		this.lead = lead;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	public void setType(String type){
		if(type!=null){
			type = type.toUpperCase();
		}
		this.type = type;
	}
	
	public String getType(){
		return this.type;
	}

	public Timestamp getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public BigInteger getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(BigInteger updateUserId) {
		this.updateUserId = updateUserId;
	}
	
	public String getCassStreetAddress1() {
		return cassStreetAddress1;
	}

	public void setCassStreetAddress1(String cassStreetAddress1) {
		this.cassStreetAddress1 = cassStreetAddress1;
	}

	public String getCassStreetAddress2() {
		return cassStreetAddress2;
	}

	public void setCassStreetAddress2(String cassStreetAddress2) {
		this.cassStreetAddress2 = cassStreetAddress2;
	}

	public String getCassCity() {
		return cassCity;
	}

	public void setCassCity(String cassCity) {
		this.cassCity = cassCity;
	}

	public String getCassState() {
		return cassState;
	}

	public void setCassState(String cassState) {
		this.cassState = cassState;
	}

	public String getCassCountry() {
		return cassCountry;
	}

	public void setCassCountry(String cassCountry) {
		this.cassCountry = cassCountry;
	}

	public String getCassPostalCode() {
		return cassPostalCode;
	}

	public void setCassPostalCode(String cassPostalCode) {
		this.cassPostalCode = cassPostalCode;
	}

	public BigInteger getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(BigInteger transactionId) {
		this.transactionId = transactionId;
	}

	/************************************************************************************************
	 * Persistence hooks
	 ************************************************************************************************/
	
	@PrePersist
    public void onPrePersist() {
		
	}

	@PreUpdate
    public void onPreUpdate() {
    	
    	
    }
       
    @PreRemove
    public void onPreRemove() { 
    	
    	
    }

}

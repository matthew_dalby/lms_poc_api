package progreso.lms.persistence.entity;

public class Preference {

	private String preferredContact;
	
	private String preferredLanguage;
	
	private Integer preferredTime;
	
	private boolean doNotAutodialCall;
	
	private boolean doNotEmail;
	
	private boolean doNotMail;
	
	private boolean doNotManualCall;
	
	private boolean doNotSms;
	
	private boolean privacyOptOut;

	public String getPreferredContact() {
		return preferredContact;
	}

	public void setPreferredContact(String preferredContact) {
		this.preferredContact = preferredContact;
	}

	public String getPreferredLanguage() {
		return preferredLanguage;
	}

	public void setPreferredLanguage(String preferredLanguage) {
		this.preferredLanguage = preferredLanguage;
	}

	public Integer getPreferredTime() {
		return preferredTime;
	}

	public void setPreferredTime(Integer preferredTime) {
		this.preferredTime = preferredTime;
	}

	public boolean isDoNotAutodialCall() {
		return doNotAutodialCall;
	}

	public void setDoNotAutodialCall(boolean doNotAutodialCall) {
		this.doNotAutodialCall = doNotAutodialCall;
	}

	public boolean isDoNotEmail() {
		return doNotEmail;
	}

	public void setDoNotEmail(boolean doNotEmail) {
		this.doNotEmail = doNotEmail;
	}

	public boolean isDoNotMail() {
		return doNotMail;
	}

	public void setDoNotMail(boolean doNotMail) {
		this.doNotMail = doNotMail;
	}

	public boolean isDoNotManualCall() {
		return doNotManualCall;
	}

	public void setDoNotManualCall(boolean doNotManualCall) {
		this.doNotManualCall = doNotManualCall;
	}

	public boolean isDoNotSms() {
		return doNotSms;
	}

	public void setDoNotSms(boolean doNotSms) {
		this.doNotSms = doNotSms;
	}

	public boolean isPrivacyOptOut() {
		return privacyOptOut;
	}

	public void setPrivacyOptOut(boolean privacyOptOut) {
		this.privacyOptOut = privacyOptOut;
	}

	
}

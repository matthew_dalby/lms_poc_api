package progreso.lms.persistence.entity;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PersistenceContext;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Size;



import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

//@PersistenceContext(unitName="pu-lms")
@JsonInclude(Include.NON_NULL)	// Required for ?
@Entity
@Table(name="lead_profile", schema="lead")
public class LeadProfile {
		
	private static final short LEAD_STATUS_UNKNOWN = 1;
	private static final short LEAD_STATUS_OPTED_OUT = 2;
	private static final short LEAD_STATUS_STARTED = 3;
	private static final short LEAD_STATUS_RPC = 4;
	private static final short LEAD_STATUS_NO_CONTACT = 5;
	
	/************************************************************************************************
	 * Attributes
	 ************************************************************************************************/
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="lead_id")
	private Long leadId;
	
	//@OneToMany(mappedBy="lead", fetch = FetchType.LAZY)
	@Transient
	private List<Address>addresses;
	
	//@Column(name="customer_id")
	private BigInteger customerId;
	
	//@Column(name="create_user_id")
	private BigInteger createUserId;
	
	@Temporal(TemporalType.TIMESTAMP)
	//@Column(name="created_at")
	private Date createdAt;
	
	//@Column(name="created_channel_id")
	private BigInteger createdChannelId;
	
	//@Column(name="date_of_birth")
	private Date dateOfBirth;
	
	@Size( max=255, message="Value for field 'emailAddress' A cannot exceed 255 characters.")
	//@Column(name="email_address")
	private String emailAddress;
	
	//@Pattern(regexp = "/[a-zA-Z]+/")
	@Size(min=1, max=255, message="Value for field 'firstName' Must be betweena and 255 characters.")
	//@Column(name="first_name")
	private String firstName;
	
	@Size( min=1, max=255, message="Value for field 'lastName' A cannot exceed 255 characters.")
	//@Column(name="last_name")
	private String lastName;				

	/*
	@ManyToOne
	@JoinColumn(name = "lead_type")
	@JsonBackReference
	private PLeadType leadType;
	*/
	
	@Size( max=255, message="Value for field 'maternalName' A cannot exceed 255 characters.")
	//@Column(name="maternal_name")
	private String maternalName;
	
	@Size( max=255, message="Value for field 'middleName' A cannot exceed 255 characters.")
	//@Column(name="middle_name")
	private String middleName;

	//@OneToMany(mappedBy="lead", fetch = FetchType.LAZY)
	@Transient
	private List<PhoneNumber>phoneNumbers;

	//@OneToMany(mappedBy="lead", fetch = FetchType.LAZY)
	@Transient
	private List<Note>notes;
	
	@Transient
	private Preference preference;

	/************************************************************************************************
	 * Convenience methods
	 ************************************************************************************************/
	public boolean hasAddresses(){

		if( this.addresses!=null&&this.addresses.isEmpty()==false ){
			return true;
		}

		return false;
	}
	
	public boolean hasPhoneNumbers(){

		if( this.phoneNumbers!=null&&this.phoneNumbers.isEmpty()==false ){
			return true;
		}

		return false;
	}
	
	public boolean hasNotes(){

		if( this.notes!=null&&this.notes.isEmpty()==false ){
			return true;
		}

		return false;
	}
	
	
	//BigInteger getUpdateUserId(){
	//	return new BigInteger(1234)
	//}
	
	/************************************************************************************************
	 * Getter/setters
	 ************************************************************************************************/
	public Long getLeadId() {
		return leadId;
	}

	public void setLeadId(Long leadId) {
		this.leadId = leadId;
	}

	public List<Address> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<Address> addresses) {
		this.addresses = addresses;
	}

	public BigInteger getCustomerId() {
		return customerId;
	}

	public void setCustomerId(BigInteger customerId) {
		this.customerId = customerId;
	}

	public BigInteger getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(BigInteger createUserId) {
		this.createUserId = createUserId;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public BigInteger getCreatedChannelId() {
		return createdChannelId;
	}

	public void setCreatedChannelId(BigInteger createdChannelId) {
		this.createdChannelId = createdChannelId;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/*
	public PLeadType getLeadType() {
		return leadType;
	}

	public void setLeadType(PLeadType leadType) {
		this.leadType = leadType;
	}
	*/

	public String getMaternalName() {
		return maternalName;
	}

	public void setMaternalName(String maternalName) {
		this.maternalName = maternalName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	
	public List<Note> getNotes() {
		return notes;
	}

	public void setNotes(List<Note> notes) {
		this.notes = notes;
	}

	public List<PhoneNumber> getPhoneNumbers() {
		return phoneNumbers;
	}

	public void setPhoneNumbers(List<PhoneNumber> phoneNumbers) {
		this.phoneNumbers = phoneNumbers;
	}
	
	/************************************************************************************************
	 * Persistence hooks
	 ************************************************************************************************/
	@PrePersist
    public void onPrePersist() {
		
	}
       
	public Preference getPreference() {
		return preference;
	}

	public void setPreference(Preference preference) {
		this.preference = preference;
	}

	@PreUpdate
    public void onPreUpdate() {
    	
    }
       
    @PreRemove
    public void onPreRemove() { 
    	
    }

}

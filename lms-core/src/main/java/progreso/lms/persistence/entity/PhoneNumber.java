package progreso.lms.persistence.entity;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.MapsId;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Embeddable
class PhoneNumberPK implements Serializable{ 

	@MapsId
	@Column(name="lead_id", nullable=false)
	BigInteger leadId;

	@Size( max=100, message="Value for field 'phoneNumber' A cannot exceed 100 characters.")
	@Column(name="phone_number")
	String phoneNumber;

	@Column(name="start_date")
	Date startDate;		

	@Size( max=2, message="Value for field 'type' A cannot exceed 2 characters.")
	@Column(name="type")
	String type;
	
}

//@Entity
//@Table(name="phone")
public class PhoneNumber implements Serializable{

	/************************************************************************************************
	 * Attributes
	 ************************************************************************************************/
	@EmbeddedId
	private PhoneNumberPK 	pk;		

	@Column(name="is_active")
	private boolean active;					
			
	@Column(name="created_at")
	private Timestamp createdAt;			

	@Column(name="create_user_id")
	private BigInteger createUserId;

	@Column(name="do_not_manual_call")
	private boolean doNotManualCall;
	
	@Column(name="do_not_autodial_call")
	private boolean doNotAutodialCall;
	
	@Column(name="pre_recorded_msg")		
	private boolean preRecordedMessage;
	
	@Column(name="end_date")
	private Date endDate;

	@ManyToOne
	@MapsId("leadId")
	@JoinColumn(name="lead_id", nullable=false)
	@JsonBackReference
	private LeadProfile lead;

	@Column(name="updated_at")
	private Timestamp updatedAt;		

	@Column(name="update_user_id")
	private BigInteger updateUserId;	

	
	 /************************************************************************************************
	 * Getter/setters
	 ************************************************************************************************/
    public PhoneNumberPK getPk() {
		return pk;
	}

	public void setPk(PhoneNumberPK pk) {
		this.pk = pk;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Timestamp getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public BigInteger getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(BigInteger createUserId) {
		this.createUserId = createUserId;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public LeadProfile getLead() {
		return lead;
	}

	public void setLead(LeadProfile lead) {
		this.lead = lead;
	}

	public Timestamp getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public BigInteger getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(BigInteger updateUserId) {
		this.updateUserId = updateUserId;
	}
	
	public boolean isDoNotManualCall() {
		return doNotManualCall;
	}

	public void setDoNotManualCall(boolean doNotManualCall) {
		this.doNotManualCall = doNotManualCall;
	}

	public boolean isDoNotAutodialCall() {
		return doNotAutodialCall;
	}

	public void setDoNotAutodialCall(boolean doNotAutodialCall) {
		this.doNotAutodialCall = doNotAutodialCall;
	}

	public boolean isPreRecordedMessage() {
		return preRecordedMessage;
	}

	public void setPreRecordedMessage(boolean preRecordedMessage) {
		this.preRecordedMessage = preRecordedMessage;
	}
	
	public String getType(){
		
		if( this.pk!=null ){
			return this.pk.type;
		}
		
		return "";
	}
	
	/************************************************************************************************
	 * Convenience methods
	 ************************************************************************************************/

	/************************************************************************************************
	 * Persistence hooks
	 ************************************************************************************************/
	@PrePersist
    public void onPrePersist() {
		
	}
   

	@PreUpdate
    public void onPreUpdate() {
    	
    }
       
    @PreRemove
    public void onPreRemove() { 
    	
    }
	
}

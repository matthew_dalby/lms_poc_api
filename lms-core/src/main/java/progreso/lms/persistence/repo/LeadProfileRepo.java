package progreso.lms.persistence.repo;

import java.math.BigInteger;

import javax.persistence.PersistenceContext;

import org.springframework.data.jpa.repository.JpaRepository;

import progreso.lms.persistence.entity.LeadProfile;



@PersistenceContext(unitName="pu-lms")
//@PersistenceContext(unitName="pu")
public interface LeadProfileRepo extends JpaRepository<LeadProfile, BigInteger>{
	
	/*
	@Query("select lead from com.opportun.lms.data.entity.PLeadProfile lead LEFT OUTER JOIN FETCH lead.addresses where lead.leadId=(:id)")
	public PLeadProfile findWithAddress(@Param("id")BigInteger id);
	*/
	
	/*
	@Query("select lead from com.opportun.lms.data.entity.PLeadProfile lead LEFT OUTER JOIN FETCH lead.phoneNumbers where lead.leadId=(:id)")
	public PLeadProfile findWithPhoneNumbers(@Param("id")BigInteger id);
	*/
	
	/*
	@Query("select lead from com.opportun.lms.data.entity.PLeadProfile lead LEFT OUTER JOIN FETCH lead.preference where lead.leadId=(:id)")
	public PLeadProfile findWithPreference(@Param("id")BigInteger id);
	
	@Query("select lead from com.opportun.lms.data.entity.PLeadProfile lead LEFT OUTER JOIN FETCH lead.referrers where lead.leadId=(:id)")
	public PLeadProfile findWithReferrers(@Param("id")BigInteger id);
	
	@Query("select lead from com.opportun.lms.data.entity.PLeadProfile lead LEFT OUTER JOIN FETCH lead.contactEvents where lead.leadId=(:id)")
	public PLeadProfile findWithContactEvents(@Param("id")BigInteger id);
	
	@Query("select lead from com.opportun.lms.data.entity.PLeadProfile lead LEFT OUTER JOIN FETCH lead.contactDetails where lead.leadId=(:id)")
	public PLeadProfile findWithContactDetails(@Param("id")BigInteger id);
	*/
	
	/*
	@Query("select lead from com.opportun.lms.data.entity.PLeadProfile lead LEFT OUTER JOIN FETCH lead.notes where lead.leadId=(:id)")
	public PLeadProfile findWithNotes(@Param("id")BigInteger id);
	*/
	/*
	@Query("select lead from com.opportun.lms.data.entity.PLeadProfile lead LEFT OUTER JOIN FETCH lead.preapprovalOffers where lead.leadId=(:id)")
	public PLeadProfile findWithPreapprovalOffers(@Param("id")BigInteger id);
	
	@Query("select lead from com.opportun.lms.data.entity.PLeadProfile lead LEFT OUTER JOIN FETCH lead.prospectProfiles where lead.leadId=(:id)")
	public PLeadProfile findWithProspectProfiles(@Param("id")BigInteger id);
	
	@Query("select lead from com.opportun.lms.data.entity.PLeadProfile lead LEFT OUTER JOIN FETCH lead.leadCampaignLogs where lead.leadId=(:id)")
	public PLeadProfile findWithLeadCampaignLogs(@Param("id")BigInteger id);
	
	@Query("select lead from com.opportun.lms.data.entity.PLeadProfile lead LEFT OUTER JOIN FETCH lead.marketingTags where lead.leadId=(:id)")
	public PLeadProfile findWithMarketingTags(@Param("id")BigInteger id);
	
	//@Query("select lead from com.opportun.lms.data.entity.PLeadProfile lead JOIN FETCH lead.addresses, lead.referrers, lead.contactEvents, lead.contactDetails, lead.notes, lead.phoneNumbers where lead.leadId=(:id)")
	//@Query("select lead from com.opportun.lms.data.entity.PLeadProfile lead JOIN FETCH lead.addresses, JOIN FETCH lead.notes where lead.leadId=(:id)")
	//public PLeadProfile findWithFullGraph(@Param("id")BigInteger id);
	*/
	
	
}


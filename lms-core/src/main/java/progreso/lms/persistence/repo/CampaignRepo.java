package progreso.lms.persistence.repo;

import java.math.BigInteger;

import javax.persistence.PersistenceContext;

import org.springframework.data.jpa.repository.JpaRepository;

import progreso.lms.persistence.entity.Campaign;

@PersistenceContext(unitName="pu-lms")
public interface CampaignRepo extends JpaRepository<Campaign, Long>{

}

package progreso.lms.common;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.util.Assert;

@ContextConfiguration(locations = "classpath:META-INF/test-context-service.xml")
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class})
public class ObjectMappingServiceImplTest {
	
	@Autowired
	private ObjectMappingService objectMappingService;
	
	@Test
	public void shouldMapBetweenBoAndDtoFormatsCorrectly(){
				
		
		WidgetBo widgetBo = new WidgetBo();
		WidgetDto widgetDto = new WidgetDto();
		
		String name = "sample";
		int age = 100;
		double weight = 191.2;
		
		widgetBo.setFirstName(name);
		widgetBo.setAge(age);
		widgetBo.setWeight(weight);
		
		objectMappingService.performSimpleMap(widgetBo, widgetDto);
		
		Assert.notNull(widgetDto.getFirstName());
		Assert.isTrue(widgetDto.getFirstName().equals(name));
		Assert.isTrue(widgetDto.getAge()==age);
		Assert.isTrue(widgetDto.getWeight()==weight);
	}
	
}

class WidgetBo{
	
	private String firstName;
	private int age;
	private double weight;
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public double getWeight() {
		return weight;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	
}

class WidgetDto{
	
	private String firstName;
	private int age;
	private double weight;
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public double getWeight() {
		return weight;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	
}
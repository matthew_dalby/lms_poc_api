package progreso.lms.domain;
import java.math.BigInteger;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonBackReference;

public class NoteDTO {

	private String noteId;

	private String body;

	private Timestamp createdAt;

	private BigInteger createUserId;

	private String type;

	private Timestamp updatedAt;

	private BigInteger updateUserId;

}

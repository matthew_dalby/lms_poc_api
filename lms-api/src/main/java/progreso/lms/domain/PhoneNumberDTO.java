package progreso.lms.domain;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonBackReference;


public class PhoneNumberDTO {
	
	String phoneNumber;

	Date startDate;		

	String type;
	
	private boolean active;					
			
	private Timestamp createdAt;			

	private BigInteger createUserId;

	private boolean doNotManualCall;
	
	private boolean doNotAutodialCall;
	
	private boolean preRecordedMessage;
	
	private Date endDate;

	private Timestamp updatedAt;		

	private BigInteger updateUserId;	
	
}

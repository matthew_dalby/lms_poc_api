package progreso.lms.domain;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.validation.constraints.Size;

public class LeadDTO {
	
	private Long leadId;

	private String firstName;
	
	private String lastName;
	
	private Date dateOfBirth;
	
	private String emailAddress;
	
	private String leadSource;
	
	private String maternalName;
	
	private String middleName;
	
	private String prospectFinderNumber;			

	private boolean prospectConfirmed;	
	
	private Timestamp updatedAt;			

	private BigInteger updateUserId;

	private BigInteger updatedChannelId;
	
	private BigInteger transactionId;
	
	private String preferredContact;
	
	private String preferredLanguage;
	
	private Integer preferredTime;
	
	private boolean doNotAutodialCall;
	
	private boolean doNotEmail;
	
	private boolean doNotMail;
	
	private boolean doNotManualCall;
	
	private boolean doNotSms;
	
	private boolean privacyOptOut;
	
	private AddressDTO homeAddress;
	
	private PhoneNumberDTO housePhoneNumber;
	
	private PhoneNumberDTO cellPhoneNumber;
	
	private List<NoteDTO> notes;

	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getLeadSource() {
		return leadSource;
	}
	public void setLeadSource(String leadSource) {
		this.leadSource = leadSource;
	}
	public String getMaternalName() {
		return maternalName;
	}
	public void setMaternalName(String maternalName) {
		this.maternalName = maternalName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getProspectFinderNumber() {
		return prospectFinderNumber;
	}
	public void setProspectFinderNumber(String prospectFinderNumber) {
		this.prospectFinderNumber = prospectFinderNumber;
	}
	public boolean isProspectConfirmed() {
		return prospectConfirmed;
	}
	public void setProspectConfirmed(boolean prospectConfirmed) {
		this.prospectConfirmed = prospectConfirmed;
	}
	public Timestamp getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}
	public BigInteger getUpdateUserId() {
		return updateUserId;
	}
	public void setUpdateUserId(BigInteger updateUserId) {
		this.updateUserId = updateUserId;
	}
	public BigInteger getUpdatedChannelId() {
		return updatedChannelId;
	}
	public void setUpdatedChannelId(BigInteger updatedChannelId) {
		this.updatedChannelId = updatedChannelId;
	}
	
	public Long getLeadId() {
		return leadId;
	}
	public void setLeadId(Long leadId) {
		this.leadId = leadId;
	}

	public BigInteger getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(BigInteger transactionId) {
		this.transactionId = transactionId;
	}
	public String getPreferredContact() {
		return preferredContact;
	}
	public void setPreferredContact(String preferredContact) {
		this.preferredContact = preferredContact;
	}
	public String getPreferredLanguage() {
		return preferredLanguage;
	}
	public void setPreferredLanguage(String preferredLanguage) {
		this.preferredLanguage = preferredLanguage;
	}
	public Integer getPreferredTime() {
		return preferredTime;
	}
	public void setPreferredTime(Integer preferredTime) {
		this.preferredTime = preferredTime;
	}
	public boolean isDoNotAutodialCall() {
		return doNotAutodialCall;
	}
	public void setDoNotAutodialCall(boolean doNotAutodialCall) {
		this.doNotAutodialCall = doNotAutodialCall;
	}
	public boolean isDoNotEmail() {
		return doNotEmail;
	}
	public void setDoNotEmail(boolean doNotEmail) {
		this.doNotEmail = doNotEmail;
	}
	public boolean isDoNotMail() {
		return doNotMail;
	}
	public void setDoNotMail(boolean doNotMail) {
		this.doNotMail = doNotMail;
	}
	public boolean isDoNotManualCall() {
		return doNotManualCall;
	}
	public void setDoNotManualCall(boolean doNotManualCall) {
		this.doNotManualCall = doNotManualCall;
	}
	public boolean isDoNotSms() {
		return doNotSms;
	}
	public void setDoNotSms(boolean doNotSms) {
		this.doNotSms = doNotSms;
	}
	public boolean isPrivacyOptOut() {
		return privacyOptOut;
	}
	public void setPrivacyOptOut(boolean privacyOptOut) {
		this.privacyOptOut = privacyOptOut;
	}
	public AddressDTO getHomeAddress() {
		return homeAddress;
	}
	public void setHomeAddress(AddressDTO homeAddress) {
		this.homeAddress = homeAddress;
	}
	public PhoneNumberDTO getHousePhoneNumber() {
		return housePhoneNumber;
	}
	public void setHousePhoneNumber(PhoneNumberDTO housePhoneNumber) {
		this.housePhoneNumber = housePhoneNumber;
	}
	public PhoneNumberDTO getCellPhoneNumber() {
		return cellPhoneNumber;
	}
	public void setCellPhoneNumber(PhoneNumberDTO cellPhoneNumber) {
		this.cellPhoneNumber = cellPhoneNumber;
	}
	public List<NoteDTO> getNotes() {
		return notes;
	}
	public void setNotes(List<NoteDTO> notes) {
		this.notes = notes;
	}
	public void addNote(NoteDTO note){
		if( this.notes==null ){
			this.notes = new ArrayList();
		}
		this.notes.add(note);
	}
}

package progreso.lms.service;

import java.util.List;

import progreso.lms.domain.CampaignDto;

public interface CampainService {

	public List<CampaignDto>list();
	
	public CampaignDto view(Long id);
	
}

package progreso.lms.service;

import java.math.BigInteger;
import java.util.List;

import progreso.lms.persistence.entity.LeadProfile;

public interface LeadProfileService {
	
	public List<LeadProfile>list();
	
	public LeadProfile findById(BigInteger id);
	
	public LeadProfile save(LeadProfile profile);
}

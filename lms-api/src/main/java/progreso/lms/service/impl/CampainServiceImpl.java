package progreso.lms.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import progreso.lms.common.ObjectMappingService;
import progreso.lms.domain.CampaignDto;
import progreso.lms.persistence.entity.Campaign;
import progreso.lms.persistence.repo.CampaignRepo;
import progreso.lms.service.CampainService;

@Service("CampainService")
public class CampainServiceImpl implements CampainService{

	@Autowired
	private CampaignRepo campaignRepo;

	@Autowired
	private ObjectMappingService objectMappingService;
	
	private final Logger LOG = LoggerFactory.getLogger(CampainServiceImpl.class);
	
	@Override
	public List<CampaignDto> list() {
		
		if( LOG.isDebugEnabled() ){
			LOG.debug("Listing all campaigns...");
		}
		
		List<CampaignDto> results;
		
		List<Campaign>cammpaigns = campaignRepo.findAll();
		 
		results = convertBosToDtoFormat(cammpaigns);
		
		return results;
	}

	@Override
	public CampaignDto view(Long id) {
		
		if( LOG.isDebugEnabled() ){
			LOG.debug("Retrieving campaign id=>"+ id);
		}
		
		Campaign campaign;
		campaign = this.campaignRepo.findOne(id);
		
		CampaignDto dto;
		dto = convertBoToDto(campaign);
		
		return dto;
	}
	
	List<CampaignDto>convertBosToDtoFormat(List<Campaign> campaigns){
		
		if( LOG.isDebugEnabled() ){
			LOG.debug("Converting collection between bo->dto formats");
		}
		
		List<CampaignDto> results = new ArrayList();
		
		if( campaigns==null ){
			return new ArrayList();
		}
		
		CampaignDto dto;
		for( Campaign campaign : campaigns){
			dto = new CampaignDto();
			objectMappingService.performSimpleMap(campaign, dto);
			results.add(dto);
		}
		
		return results;
	}
	
	CampaignDto convertBoToDto(Campaign campaignBo){
		if( LOG.isDebugEnabled() ){
			LOG.debug("Converting between bo->dto formats");
		}
		
		CampaignDto dto = new CampaignDto();
		objectMappingService.performSimpleMap(campaignBo, dto);
		return dto;
	}
	
	Campaign convertDtoToBo(CampaignDto campaignDto){
		if( LOG.isDebugEnabled() ){
			LOG.debug("Converting between dto->bo formats");
		}
		
		Campaign campaign = new Campaign();
		objectMappingService.performSimpleMap(campaignDto, campaign);
		return campaign;
	}
	
	
}

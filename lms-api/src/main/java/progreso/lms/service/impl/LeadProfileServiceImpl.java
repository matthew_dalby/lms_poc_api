package progreso.lms.service.impl;

import java.math.BigInteger;
import java.util.List;

import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import progreso.lms.persistence.entity.LeadProfile;
import progreso.lms.persistence.repo.LeadProfileRepo;
import progreso.lms.service.LeadProfileService;

@PersistenceContext(unitName="pu-lms")
@Service("LeadProfileService")
public class LeadProfileServiceImpl implements LeadProfileService{

	private final Logger LOG = LoggerFactory.getLogger(LeadProfileServiceImpl.class);

	@Autowired
	private LeadProfileRepo repo;
	
	public LeadProfile findById(BigInteger id) {
		if( LOG.isDebugEnabled() ){
            LOG.debug("Service method called");
        }
		
		return repo.findOne(id);
	}

	//@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW, value="lmsTransactionManager")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public LeadProfile save(LeadProfile profile) {
		if( LOG.isDebugEnabled() ){
            LOG.debug("Service method called: Persisting entity");
        }
	
		return repo.saveAndFlush(profile);
	}

	public List<LeadProfile> list() {
		if( LOG.isDebugEnabled() ){
            LOG.debug("Service method called");
        }
		return repo.findAll();
	}
	
}

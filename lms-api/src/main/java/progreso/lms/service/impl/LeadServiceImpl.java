package progreso.lms.service.impl;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import progreso.lms.common.InvalidSearchOperation;
import progreso.lms.common.LeadConstants;
import progreso.lms.common.ObjectMappingService;
import progreso.lms.common.UserIdentityService;
import progreso.lms.persistence.entity.Address;
import progreso.lms.persistence.entity.LeadProfile;
import progreso.lms.persistence.entity.Note;
import progreso.lms.persistence.entity.PhoneNumber;
import progreso.lms.domain.AddressDTO;
import progreso.lms.domain.LeadDTO;
import progreso.lms.domain.NoteDTO;
import progreso.lms.domain.PhoneNumberDTO;
import progreso.lms.service.LeadProfileService;
import progreso.lms.service.LeadService;

@Service("LeadService")
public class LeadServiceImpl implements LeadService{

	private final Logger LOG = LoggerFactory.getLogger(LeadServiceImpl.class);
	
	@Autowired
	LeadProfileService leadProfileService;
	
	@Autowired
	private ObjectMappingService objectMappingService;
	
	public List<LeadDTO> search(String firstName, String lastName) {
		if( LOG.isDebugEnabled() ){
            LOG.debug("Service method called");
        }
		
		if( searchCriteriaPresent(firstName, lastName)==false ){
			throw new InvalidSearchOperation("No search criteria provided");
		}
		
		List<LeadDTO>searchResults = new ArrayList();
		
		LeadDTO lead;
		for( int x=0;x<5;x++ ){
			lead = new LeadDTO();
			lead.setFirstName("lead-"+ x);
			lead.setLastName("last-name-"+ x);
			searchResults.add(lead);
		}
		
		return searchResults;
	}
	
	public boolean searchCriteriaPresent(String firstName, String lastName){
		if( isStringNullOrEmpty(firstName) && isStringNullOrEmpty(lastName) ){
			return false;
		}
		return true;
	}
	
	public LeadDTO create(LeadDTO lead) {
		if( LOG.isDebugEnabled() ){
            LOG.debug("Creating new lead");
        }
		
		
		LeadProfile leadProfile;
		leadProfile = convertVoToBo(lead);
		
		Timestamp createdAt = new Timestamp(Calendar.getInstance().getTimeInMillis());
		BigInteger createdUserId = UserIdentityService.getCurrentUserId();
		

		leadProfile = this.leadProfileService.save(leadProfile);
		
		lead = convertBoToVo(leadProfile);
		
		return lead;
	}
	
	public LeadDTO update(LeadDTO lead) {
		if( LOG.isDebugEnabled() ){
            LOG.debug("Updating lead");
        }
		
		LeadProfile profile = convertVoToBo(lead);
		profile = this.leadProfileService.save(profile);
		lead = convertBoToVo(profile);
		return lead;
	}
	
	public void delete(BigInteger id) {
		if( LOG.isDebugEnabled() ){
            LOG.debug("Deleting lead");
        }

	}
	
	LeadDTO convertBoToVo(LeadProfile pLead){
		
		
		if( LOG.isDebugEnabled() ){
			LOG.debug("Converting lead '"+ pLead.getFirstName() +" "+ pLead.getLastName() +"' between formats: BO->DTO");
		}
		
		LeadDTO lead = new LeadDTO();
		//lead.setLeadId(pLead.getLeadId());
		//lead.setFirstName(pLead.getFirstName());
		//lead.setLastName(pLead.getLastName());
		objectMappingService.performSimpleMap(pLead, lead);
		
		//Convert addresses
		if( pLead.hasAddresses() ){
			AddressDTO address;
			for( Address pAddress : pLead.getAddresses() ){
				
				address = new AddressDTO();
				objectMappingService.performSimpleMap(pAddress, address);
				if( pAddress.getType().equals(LeadConstants.HOUSE_TYPE_HOME) ){
					lead.setHomeAddress(address);
				}
				
			}
		}
		
		//Convert phone numbers
		if( pLead.hasPhoneNumbers() ){
			PhoneNumberDTO phoneNumber;
			for( PhoneNumber pPhoneNumber : pLead.getPhoneNumbers() ){
				phoneNumber = new PhoneNumberDTO();
				objectMappingService.performSimpleMap(pPhoneNumber, phoneNumber);
				if( pPhoneNumber.getType().equals(LeadConstants.PHONE_TYPE_HOME) ){
					lead.setHousePhoneNumber(phoneNumber);
				}
				
				if( pPhoneNumber.getType().equals(LeadConstants.PHONE_TYPE_CELL) ){
					lead.setCellPhoneNumber(phoneNumber);
				}
			}
		}
		
		//Convert notes
		if( pLead.hasNotes() ){
			NoteDTO note;
			for( Note pNote : pLead.getNotes() ){
				note = new NoteDTO();
				objectMappingService.performSimpleMap(pNote, note);
				lead.addNote(note);
			}
			
		}
		
		//Convert preference data from separate entity into part of the parent lead object. The actual profile object 
		//will contain the preferences as a separate object. Here we merge it into the top level lead entity.
		if( pLead.getPreference()!=null ){
			lead.setDoNotAutodialCall(pLead.getPreference().isDoNotAutodialCall());
			lead.setDoNotEmail(pLead.getPreference().isDoNotEmail());
			lead.setDoNotMail(pLead.getPreference().isDoNotMail());
			lead.setDoNotManualCall(pLead.getPreference().isDoNotManualCall());
			lead.setDoNotSms(pLead.getPreference().isDoNotSms());
			lead.setPrivacyOptOut(pLead.getPreference().isPrivacyOptOut());
		}

		return lead;
	}
	
	LeadProfile convertVoToBo(LeadDTO lead){
		
		LeadProfile leadProfile = new LeadProfile();
		leadProfile.setLeadId(lead.getLeadId());
		leadProfile.setFirstName(lead.getFirstName());
		leadProfile.setLastName(lead.getLastName());
		//TODO: Convert attributes. 
		
		return leadProfile;
	}

	List<LeadDTO>convertLeadProfilesToLeads(List<LeadProfile>leadProfiles){
		
		List<LeadDTO>leads = new ArrayList();
		LeadDTO lead;
		if( leadProfiles!=null && leadProfiles.isEmpty()==false ){
			for( LeadProfile leadProfile: leadProfiles ){
				lead = convertBoToVo(leadProfile);
				leads.add(lead);
			}
		}
		
		return leads;
	}
	
	boolean isStringNullOrEmpty(String s){
		if( s==null ){
			return true;
		}
		
		if( StringUtils.isBlank(s) ){
			return true;
		}
			
		
		return false;
	}

	public LeadDTO view(BigInteger id) {
		if( LOG.isDebugEnabled() ){
            LOG.debug("Retrieving lead");
        }
		
		LeadProfile leadProfile;
		leadProfile = this.leadProfileService.findById(id);
		LeadDTO lead = convertBoToVo(leadProfile);
		return lead;
	}

	public List<LeadDTO> list() {
		if( LOG.isDebugEnabled() ){
            LOG.debug("Listing leads");
        }
		
		List<LeadProfile>leadProfiles = this.leadProfileService.list();
		List<LeadDTO>leads = convertLeadProfilesToLeads(leadProfiles);
		
		return leads;
	}
	
}

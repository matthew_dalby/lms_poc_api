package progreso.lms.service.impl;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import progreso.lms.service.LocalEnvironmentService;

@Service("LocalEnvironmentService")
public class LocalEnvironmentServiceImpl implements LocalEnvironmentService{

	@Autowired
	private Environment environment;

	@Value("app_name")
	String appName;
	
	public HashMap<String,String> getEnvironmentConfig() {
		
		HashMap<String,String> environmentDetails = new HashMap();
		environmentDetails.put("Active Spring Profile(s)", convertProfilesToArray(environment.getActiveProfiles()));
		environmentDetails.put("mysql connection","not defined");
		environmentDetails.put("app.name", appName);
		//environmentDetails.put("globalProperty", globalProperty);

		return environmentDetails;
	}
	
	String convertProfilesToArray(String[] profiles){
		StringBuffer result = new StringBuffer();
		
		for(int x=0;x<profiles.length;x++){
			result.append(profiles[x]);
		}
		
		return result.toString();
	}
	
}

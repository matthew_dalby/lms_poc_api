package progreso.lms.service;

import java.util.HashMap;

public interface LocalEnvironmentService {

	public HashMap<String, String> getEnvironmentConfig();
	
}

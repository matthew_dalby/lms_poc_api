package progreso.lms.service;

import java.math.BigInteger;
import java.util.List;

import progreso.lms.domain.LeadDTO;

/**
 * Service responsible for providing lead related functionality.
 * 
 * @author matthew.dalby
 *
 */
public interface LeadService {

	public List<LeadDTO>search(String firstName, String lastName);
	
	public LeadDTO create(LeadDTO lead);
	
	public LeadDTO update(LeadDTO lead);
	
	public void delete(BigInteger id);
	
	public LeadDTO view(BigInteger id);
	
	public boolean searchCriteriaPresent(String firstName, String lastName);
	
	public List<LeadDTO>list();
	
}

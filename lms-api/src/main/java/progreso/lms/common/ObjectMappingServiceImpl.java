package progreso.lms.common;
import java.lang.reflect.Field;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.util.ReflectionUtils;

/**
 * ObjectMappingServiceImpl.java
 * 
 * 		This class is intended to perform mapping operations between value objects and persistent entities. This task, 
 * when performed manually is typically tedious and error prone. The goal of this service is to streamline mapping type 
 * tasks as much as possible. 
 *  
 * 
 * @author mdalby
 *
 */
@Service("ObjectMappingService")
public class ObjectMappingServiceImpl implements ObjectMappingService{

	/**
	 * Copy all primative attributes from one object to another. Method will accept either value objects 
	 * or business objects as the target.
	 * 
	 * @param source
	 * @param target
	 * @return
	 */
	public Object performSimpleMap(Object source, Object target) {
		
		try{
			
			if( source==null || target==null ){
				return null;
			}
			
			Field[] fields = source.getClass().getDeclaredFields();
			Field curSourceField, targetField;
			String curFieldName;
			Object curFieldValue;
			if( fields!=null ){
				for( int x=0;x<fields.length;x++ ){
					curSourceField = fields[x];
					curFieldName = curSourceField.getName();
					
					ReflectionUtils.makeAccessible(curSourceField);
					targetField = target.getClass().getDeclaredField(curFieldName);
					ReflectionUtils.makeAccessible(targetField);

					if( isBasicAttribute(curSourceField) ){
						ReflectionUtils.setField(targetField, target, curSourceField.get(source));
					}					
				}
			}
		
		}
		catch (NoSuchFieldException e) {
			//throw new RuntimeException(e);
		}
		catch(IllegalAccessException e){
			throw new RuntimeException(e);
		} 
		catch (SecurityException e) {
			throw new RuntimeException(e);
		}
		
		return target;
	}
	
	/**
	 * 
	 * @param field
	 * @return
	 */
	public boolean isBasicAttribute(Field field){
		
		if ( field.getType().isPrimitive() ){
			return true;
		}
		
		Class c = field.getType();
		
		if( field.getType().equals(String.class) ){ 
			return true;
		}
		
		if( field.getType().equals(Boolean.class) ){ 
			return true;
		}
		
		if( field.getType().equals(Long.class) ){ 
			return true;
		}
		
		if( field.getType().equals(Integer.class) ){ 
			return true;
		}
		
		if( field.getType().equals(Date.class) ){ 
			return true;
		}
				
		return false;		
	}
	
	/**
	 * 
	 * @param source
	 * @param target
	 * @return
	 */
	public Object performSimpleMap(Object source, Object target, List<String>attributes) {
			
		//	
			if( attributes!=null ){
			
				//Convert list into map for convenience.
					HashMap<String, String>attributesMap = new HashMap();
					for( int x=0;x<attributes.size();x++ ){
						attributesMap.put(attributes.get(x), null);
					}
					
				//
				try{
					
					if( source==null || target==null ){
						return null;
					}
					
					Field[] fields = source.getClass().getDeclaredFields();
					Field curSourceField, targetField;
					String curFieldName;
					Object curFieldValue;
					if( fields!=null ){
						for( int x=0;x<fields.length;x++ ){
							curSourceField = fields[x];
							curFieldName = curSourceField.getName();
							
							if( attributesMap.containsKey(curFieldName)==false ){
								continue;
							}
							
							ReflectionUtils.makeAccessible(curSourceField);
							targetField = target.getClass().getDeclaredField(curFieldName);
							ReflectionUtils.makeAccessible(targetField);
							
							ReflectionUtils.setField(targetField, target, curSourceField.get(source));
						}
					}
				
				}
				catch(IllegalAccessException e){
					throw new RuntimeException(e);
				} catch (NoSuchFieldException e) {
					throw new RuntimeException(e);
				} catch (SecurityException e) {
					throw new RuntimeException(e);
				}
				
			}
		
		
		return target;
	}
	
	/*
	public Object mapObject(Object source, Object target) {
		// TODO Auto-generated method stub
		return null;
	}
	*/

}

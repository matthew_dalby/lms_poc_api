package progreso.lms.common;

public class InvalidSearchOperation extends RuntimeException{
	
	public InvalidSearchOperation(String msg){
		super(msg);
	}
	
}

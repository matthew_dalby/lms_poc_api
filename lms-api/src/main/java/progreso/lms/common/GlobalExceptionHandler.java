package progreso.lms.common;

import java.util.HashMap;
import java.util.List;

import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {

	/**
	 * Handler for operations where request is placed for which validation exceptions 
	 * exists either for attribute level or business rule level.
	 * 
	 * 
	 */
	@ExceptionHandler(EntityNotFoundException.class)
	public HashMap<String, Object> handleInvalidRestOperationRequest(EntityNotFoundException exception){
		
		HashMap<String, Object> response = new HashMap();
		
		//if( exception.)
		//TODO: Handle exception
		
		return response;
	}
	
	@ExceptionHandler(EntityValidationException.class)
public HashMap<String, Object> handleInvalidRestOperationRequest(EntityValidationException exception){
		
		HashMap<String, Object> response = new HashMap();
		
		HashMap fieldLevelMessageDetails = new HashMap();
		
		List<FieldError>fieldErrors = exception.getFieldLevelExceptions();
		for( FieldError fieldError : fieldErrors ){
			fieldLevelMessageDetails.put(fieldError.getField(), fieldError.getDefaultMessage());
		}
		
		response.put("status","validationFailure");
		response.put("fieldErrors", fieldLevelMessageDetails);
		return response;
	}
	
}

package progreso.lms.common;


import java.util.List;

import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

public class EntityValidationException extends RuntimeException{

	private Object rootEntity;
	
	private BindingResult result;
	
	public EntityValidationException(){
		
	}
	
	public EntityValidationException(Object o, BindingResult result){
		
		this.rootEntity = o;
		this.result = result;
		
	}
	
	public List<FieldError> getFieldLevelExceptions(){
		if( this.result!=null ){
			return this.result.getFieldErrors();					
		}
		return null;
	}
}

package progreso.lms.common;

import java.lang.reflect.Field;
import java.util.List;

/**
 * ObjectMappingService.java
 * 
 * Responsible for mapping between persistent and value object domain objects.
 * 
 * @author mdalby
 *
 */
public interface ObjectMappingService {

	/**
	 * Given an object with populated values (source) and another empty object (target), clone the values present 
	 * in the source object into the target. This method will be a fully recursive, deep fetch operation, where the 
	 * entire graph is copied.
	 * 
	 * 
	 * @param source
	 * @param target
	 */
	//public Object mapObject(Object source, Object target);

	/**
	 * Copy all primative attributes from one object to another. Method will accept either value objects 
	 * or business objects as the target.
	 * 
	 * @param source
	 * @param target
	 * @return
	 */
	public Object performSimpleMap(Object source, Object target);
	
	/**
	 * 
	 * @param source
	 * @param target
	 * @return
	 */
	public Object performSimpleMap(Object source, Object target, List<String>attributes);
	
	/**
	 * 
	 * @param field
	 * @return
	 */
	public boolean isBasicAttribute(Field field);
	
}
